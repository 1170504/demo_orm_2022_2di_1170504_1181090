package demo_ORM.apresentacao;

import demo_ORM.GrupoAutomovel;
import demo_ORM.aplicacao.GrupoAutomovelController;
import demo_ORM.util.Console;

import java.util.List;

/**
 *
 * @author mcn
 */
public class GrupoAutomovelUI {

    private final GrupoAutomovelController controller = new GrupoAutomovelController();

    public void registarGA() {
        System.out.println("*** Registo Grupo Automóvel ***\n");
        String nome = Console.readLine("Nome:");
        int portas = Console.readInteger("Número de portas");
        String classe = Console.readLine("Classe:");
        GrupoAutomovel grupoAutomovel = controller.
                registarGrupoAutomóvel(nome, portas, classe);
        System.out.println("Grupo Automóvel" + grupoAutomovel);
    }
    public void listarGAs() {
        GrupoAutomovelController gac = new GrupoAutomovelController();
        List<GrupoAutomovel> listarGruposAutomoveis = gac.listarGruposAutomoveis();
        for (GrupoAutomovel ga : listarGruposAutomoveis) {
            System.out.println(ga.toString());
        }
    }

    public void procurarGAPorID(long id) {
        GrupoAutomovelController gac = new GrupoAutomovelController();
        GrupoAutomovel procurarGrupoAutomovel = gac.procurarGrupoAutomovel(id);
        if (procurarGrupoAutomovel == null) {
            System.out.println("Nao ha nenhum grupo automovel com o id" + id);
        } else {
            System.out.println(procurarGrupoAutomovel);
        }
    }
}


