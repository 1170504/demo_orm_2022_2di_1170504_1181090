package demo_ORM;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Automovel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int kms;

    private int teste;

    public void setKms(int kms) {
        this.kms = kms;
    }

    protected Automovel(){}

}
