package demo_ORM;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class GrupoAutomovel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String nome;
    private String classe;
    private int portas;



    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    protected GrupoAutomovel(){}

    public GrupoAutomovel(String nome, int portas, String classe){
        this.nome  = nome;
        this.portas = portas;
        this.classe = classe;
    }

    @Override
    public String toString() {
        return "GrupoAutomovel{" +
                "classe='" + classe + '\'' +
                '}';
    }
}